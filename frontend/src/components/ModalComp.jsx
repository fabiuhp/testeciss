import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    FormControl,
    FormLabel,
    Input,
    Box,
} from "@chakra-ui/react";
import { useState } from "react";
import axios from "axios";

const ModalComp = ({ data, setData, dataEdit, isOpen, onClose }) => {
    const [name, setName] = useState(dataEdit.name || "");
    const [lastName, setLastName] = useState(dataEdit.lastName || "");
    const [email, setEmail] = useState(dataEdit.email || "");
    const [nis, setNis] = useState(dataEdit.nis || "");

    const handleSave = async () => {
        debugger;

        if (!name || !email) return;

        if (emailAlreadyExists()) {
            return alert("E-mail já cadastrado!");
        }

        if (Object.keys(dataEdit).length) {
            data[dataEdit.index] = { name, lastName, email, nis };
        }


        try {
            const response = await axios.post('http://localhost:8080/funcionarios', {
                name,
                lastName,
                email,
                nis
            });

            const newDataArray = !Object.keys(dataEdit).length
                ? [...(data ? data : []), { name, lastName, email, nis }]
                : [...(data ? data : [])];

            setData(newDataArray);

            onClose();
        } catch (error) {
            alert("Não foi possível salvar o cadastro.")
        }

        onClose();
    };

    const emailAlreadyExists = () => {
        if (dataEdit.email !== email && data?.length) {
            return data.find((item) => item.email === email);
        }

        return false;
    };

    return (
        <>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Cadastro de Clientes</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <FormControl display="flex" flexDir="column" gap={4}>
                            <Box>
                                <FormLabel>Nome</FormLabel>
                                <Input
                                    type="text"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </Box>
                            <Box>
                                <FormLabel>Sobrenome</FormLabel>
                                <Input
                                    type="text"
                                    value={lastName}
                                    onChange={(e) => setLastName(e.target.value)}
                                />
                            </Box>
                            <Box>
                                <FormLabel>E-mail</FormLabel>
                                <Input
                                    type="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </Box>
                            <Box>
                                <FormLabel>NIS</FormLabel>
                                <Input
                                    type="text"
                                    value={nis}
                                    onChange={(e) => setNis(e.target.value)}
                                />
                            </Box>
                        </FormControl>
                    </ModalBody>

                    <ModalFooter justifyContent="start">
                        <Button colorScheme="green" mr={3} onClick={handleSave}>
                            SALVAR
                        </Button>
                        <Button colorScheme="red" onClick={onClose}>
                            CANCELAR
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    );
};

export default ModalComp;