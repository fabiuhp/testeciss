package com.fabiopereira.testeciss.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    @Size(min = 2, max = 30)
    private String name;
    @Size(min = 2, max = 50)
    private String lastName;
    @Email
    private String email;
    @Pattern(regexp = "^[0-9]{11}$", message = "Apenas 11 números são permitidos.")
    private String nis;
}
