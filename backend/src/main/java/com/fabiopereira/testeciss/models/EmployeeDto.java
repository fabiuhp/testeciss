package com.fabiopereira.testeciss.models;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeDto {
    @NotBlank
    @Size(min = 2, max = 30)
    private String name;
    @Size(min = 2, max = 50)
    private String lastName;
    @Email
    private String email;
    @Pattern(regexp = "^[0-9]{11}$", message = "Apenas 11 números são permitidos.")
    private String nis;
}
