package com.fabiopereira.testeciss.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeListDto {
    private Long id;
    private String name;
    private String lastName;
    private String email;
}
