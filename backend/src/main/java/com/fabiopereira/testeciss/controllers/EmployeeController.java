package com.fabiopereira.testeciss.controllers;

import com.fabiopereira.testeciss.models.Employee;
import com.fabiopereira.testeciss.models.EmployeeDto;
import com.fabiopereira.testeciss.models.EmployeeListDto;
import com.fabiopereira.testeciss.services.EmployeeService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/funcionarios")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final ModelMapper modelMapper;

    public EmployeeController(EmployeeService employeeService, ModelMapper modelMapper) {
        this.employeeService = employeeService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public ResponseEntity<Page<EmployeeListDto>> findAll(Pageable pageable) {
        Page<Employee> employees = employeeService.findAll(pageable);
        Page<EmployeeListDto> employeeDtos = employees.map(employee -> modelMapper.map(employee, EmployeeListDto.class));
        return ResponseEntity.ok(employeeDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeListDto> findById(@PathVariable Long id) {
        Optional<Employee> employee = employeeService.findById(id);

        return employee.map(value -> ResponseEntity.ok(modelMapper.map(value, EmployeeListDto.class)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<EmployeeListDto> create(@Valid @RequestBody EmployeeDto employeeDto) {
        Employee newEmployee = employeeService.create(modelMapper.map(employeeDto, Employee.class));
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(newEmployee.getId())
                .toUri();
        return ResponseEntity.created(location).body(modelMapper.map(newEmployee, EmployeeListDto.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeListDto> updateEmployee(@PathVariable Long id, @RequestBody @Valid EmployeeDto employeeDto) {
        return ResponseEntity.ok(employeeService.update(id, employeeDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
